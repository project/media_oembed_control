# Media oEmbed Control

Allows for controlling media assets embedded with oEmbed. Currently providing a field formatter option for video autoplay (YouTube, Vimeo).

## Installation and basic usage

* Add the module as usual and activate.
* Configure additional settings in the field display settings.

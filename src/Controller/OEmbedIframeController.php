<?php

namespace Drupal\media_oembed_control\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\media\Controller\OEmbedIframeController as BaseController;
use Drupal\media\IFrameMarkup;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller which renders an oEmbed resource in a bare page (without blocks)
 * along with additional video settings.
 *
 */
class OEmbedIframeController extends BaseController {

  /**
   * Renders an oEmbed resource.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Will be thrown if the 'hash' parameter does not match the expected hash
   *   of the 'url' parameter.
   */
  public function render(Request $request) {
    $response = parent::render($request);
    $media_oembed_control = $request->query->get('media_oembed_control', NULL);
    if (!empty($media_oembed_control)) {
      $markup = $response->getContent();
      $dom = Html::load($markup);
      $tags = $dom->getElementsByTagName('iframe');
      if ($tags->length > 0) {
        if ($media_oembed_control['provider_name'] == 'YouTube' || $media_oembed_control['provider_name'] === 'Vimeo') {
          $tag = $tags->item(0);
          $src = $tag->getAttribute('src');
          $url_parts = UrlHelper::parse($src);

          // Only enable jsapi for YouTube.
          if ($media_oembed_control['provider_name'] === 'YouTube') {
            $url_parts['query']['enablejsapi'] = TRUE;
          }

          // Set additional options
          if (isset($media_oembed_control['settings']['video_autoplay']) && $media_oembed_control['settings']['video_autoplay']) {
            $url_parts['query']['autoplay'] = TRUE;
          }
          if (isset($media_oembed_control['settings']['video_background']) && $media_oembed_control['settings']['video_background']) {
            $url_parts['query']['background'] = 1;
          }
          $new_url = Url::fromUri($url_parts['path'], [
            'query' => $url_parts['query'],
            'fragment' => $url_parts['fragment'],
          ]);

          /*
           * We use a str_replace here for the overall markup instead of saving
           * the entire markup again because the markup isn't valid HTML at this
           * phase (because of the css placeholders) so the DomDocument will
           * mess up the parsing.
           */
          $old_tag = $dom->saveHTML($tag);
          $tag->setAttribute('src', $new_url->toString());
          $new_tag = $dom->saveHTML($tag);
          $markup = str_replace($old_tag, $new_tag, $markup);
        }
      }
      $response->setContent($markup);
    }

    return $response;
  }

}
